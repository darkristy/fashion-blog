import React, { ReactNode } from 'react'
import GlobalStyles from '../utils/global-styles'
import { Global } from '@emotion/core'

interface LayoutProps {
  children: ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Global styles={GlobalStyles} />
      <main>{children}</main>
    </>
  )
}

export default Layout
