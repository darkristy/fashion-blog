import styled from '@emotion/styled'

export const Container = styled.div`
  margin: 0 auto;
  width: 85%;
  position: relative;
  max-width: 1920px;
`
