export default {
  colors: {
    text: 'black',
    background: '#fff',
    primary: '#EFB800',
    secondary: '#ff6347',
  },
}
